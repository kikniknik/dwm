/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 4;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 22;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "Source Code Pro-10", "emoji-11", "icon-11", "Symbols Nerd Font-13" };
static const char dmenufont[]       = "sans serif";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_red[]    	    = "#770000";
static const unsigned int baralpha = 0xd0;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4,  "#330033", "#FF02EA"  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* staticstatus */
static const int statmonval = 0;

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */

 	/* class,        instance,          title,                tags mask, iscentered, isfloating, canfocus, monitor, border width */
	{ "Gimp",        NULL,              NULL,                 0,         0,          1,           1,          -1,   -1 },
	{ "Pavucontrol", NULL,              NULL,                 0,         1,          1,           1,          -1,   -1 },
	{ "KeePassXC",   NULL,              NULL,                 0,         0,          1,           1,          1,    -1 },
	{ "Thunderbird", NULL,              NULL,                 1 << 2,    0,          0,           1,          0,    -1 },
	{ "Pidgin",      NULL,              NULL,                 1 << 1,    0,          0,           1,          1,    -1 },
	{ NULL,          NULL,              "htop",               0,         1,          1,           1,          -1,   -1 },
	{ "WebTorrent",  NULL,              NULL,                 0,         0,          1,           1,          -1,   -1 },
	{ "firefox",     NULL,              "Picture-in-Picture", 0,         0,          1,           1,          -1,   -1 },
	{ "st",          "timer",           NULL,                 0,         0,          1,           1,          -1,   -1 },
	{ "st",          "bc",              NULL,                 0,         0,          1,           1,          -1,   -1 },
	{ "Alacritty",   "Alacritty-float", NULL,                 0,         1,          1,           1,          -1,   -1 },
	{ "Neko",        "Neko",            NULL,                 ~0,        0,          1,           0,          -1,   0 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      comboview,      {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      combotag,       {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

#define STATUSBAR "dwmblocks"

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define DMENUCMD(cmd) { .v = (const char*[]){ cmd, "-m", dmenumon, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
/* static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL }; */

static const Key keys[] = {
	/* modifier,                    key,          function,       argument */
	{ MODKEY|ControlMask,           XK_p,         spawn,          DMENUCMD("dmenu_run") },
	/* { MODKEY|ShiftMask,             XK_p,         spawn,          DMENUCMD("dmenu_term_run") }, */
	/* { MODKEY|ControlMask,           XK_Delete,    spawn,          DMENUCMD("powermenu") }, */
	/* { MODKEY,                       XK_r,         spawn,          DMENUCMD("editrecentmenu") }, */
	/* { MODKEY,                       XK_semicolon, spawn,          DMENUCMD("emojimenu") }, */
	/* { MODKEY,                       XK_o,         spawn,          SHCMD("openmenu") }, */
	/* { MODKEY,                       XK_c,         spawn,          SHCMD("clipmenu") }, */
	{ MODKEY|ShiftMask,             XK_b,         togglebar,      {0} },
	{ MODKEY,                       XK_j,         focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_grave,     focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,         focusstack,     {.i = -1 } },
	{ MODKEY|ControlMask,           XK_Return,    focusmaster,    {0} },
	{ MODKEY,                       XK_n,         shiftview,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_n,         shiftview,      {.i = -1 } },
	/* { MODKEY,                       XK_b,         shiftview,      {.i = -1 } }, */
	{ MODKEY,                       XK_i,         incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_i,         incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,         setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,         setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return,    zoom,           {0} },
	{ MODKEY,                       XK_Tab,       view,           {0} },
	{ MODKEY,                       XK_q,         killclient,     {0} },
	{ MODKEY,                       XK_t,         setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_g,         setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,         setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_u,         setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_y,         setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,     setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,     togglefloating, {0} },
	{ MODKEY,                       XK_0,         view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,         tag,            {.ui = ~0 } },
	{ MODKEY,                    XK_comma,     focusmon,       {.i = -1 } },
	{ MODKEY,                    XK_period,    focusmon,       {.i = +1 } },
	/* { MODKEY,                       XK_comma,     focusfixedmon,  {.i = 0 } }, */
	/* { MODKEY,                       XK_period,    focusfixedmon,  {.i = 1 } }, */
	{ MODKEY|ShiftMask,             XK_comma,     tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,    tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_q,         quit,           {0} },

        { Mod4Mask,                       XK_Down,  moveresize,     {.v = "0x 25y 0w 0h" } },
        { Mod4Mask,                       XK_Up,    moveresize,     {.v = "0x -25y 0w 0h" } },
        { Mod4Mask,                       XK_Right, moveresize,     {.v = "25x 0y 0w 0h" } },
        { Mod4Mask,                       XK_Left,  moveresize,     {.v = "-25x 0y 0w 0h" } },
        { Mod4Mask|ShiftMask,             XK_Down,  moveresize,     {.v = "0x 0y 0w 25h" } },
        { Mod4Mask|ShiftMask,             XK_Up,    moveresize,     {.v = "0x 0y 0w -25h" } },
        { Mod4Mask|ShiftMask,             XK_Right, moveresize,     {.v = "0x 0y 25w 0h" } },
        { Mod4Mask|ShiftMask,             XK_Left,  moveresize,     {.v = "0x 0y -25w 0h" } },
        { Mod4Mask|ControlMask,           XK_Up,    moveresizeedge, {.v = "t"} },
        { Mod4Mask|ControlMask,           XK_Down,  moveresizeedge, {.v = "b"} },
        { Mod4Mask|ControlMask,           XK_Left,  moveresizeedge, {.v = "l"} },
        { Mod4Mask|ControlMask,           XK_Right, moveresizeedge, {.v = "r"} },
        { Mod4Mask|ControlMask|ShiftMask, XK_Up,    moveresizeedge, {.v = "T"} },
        { Mod4Mask|ControlMask|ShiftMask, XK_Down,  moveresizeedge, {.v = "B"} },
        { Mod4Mask|ControlMask|ShiftMask, XK_Left,  moveresizeedge, {.v = "L"} },
        { Mod4Mask|ControlMask|ShiftMask, XK_Right, moveresizeedge, {.v = "R"} },

	{ 0,           XF86XK_AudioRaiseVolume, spawn, SHCMD("mltctl volume +7") },
	{ ShiftMask,   XF86XK_AudioRaiseVolume, spawn, SHCMD("mltctl volume +2") },
	{ 0,           XF86XK_AudioLowerVolume, spawn, SHCMD("mltctl volume -7") },
	{ ShiftMask,   XF86XK_AudioLowerVolume, spawn, SHCMD("mltctl volume -2") },
	{ 0,           XF86XK_AudioMute,        spawn, SHCMD("mltctl volume 0") },
	{ ControlMask, XF86XK_AudioRaiseVolume, spawn, SHCMD("mltctl mic-volume +7") },
	{ ControlMask, XF86XK_AudioLowerVolume, spawn, SHCMD("mltctl mic-volume -7") },
	{ ControlMask, XF86XK_AudioMute,        spawn, SHCMD("mltctl mic-volume 0") },
	{ 0,           XF86XK_AudioMicMute,     spawn, SHCMD("mltctl mic-volume 0") },
	{ 0,           XF86XK_AudioPlay,        spawn, SHCMD("mpvcmd -e 'cycle pause'") },
	{ 0,           XF86XK_AudioPause,       spawn, SHCMD("mpvcmd -e 'cycle pause'") },
	{ 0,           XF86XK_AudioStop,        spawn, SHCMD("mpvcmd -e 'cycle pause'") },
	{ 0,           XF86XK_AudioPrev,        spawn, SHCMD("mltctl handle-bt-prev") },
	{ 0,           XF86XK_AudioNext,        spawn, SHCMD("mltctl handle-bt-next") },


	TAGKEYS(XK_1, 0)
	TAGKEYS(XK_2, 1)
	TAGKEYS(XK_3, 2)
	TAGKEYS(XK_4, 3)
	TAGKEYS(XK_5, 4)
	TAGKEYS(XK_6, 5)
	TAGKEYS(XK_7, 6)
	TAGKEYS(XK_8, 7)
	TAGKEYS(XK_9, 8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigstatusbar,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigstatusbar,   {.i = 5} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

